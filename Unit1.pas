unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TeEngine, Series, ExtCtrls, TeeProcs, Chart, Math;

type
  TPart = record
    Ur: extended;             // �������� �������
    Uz: extended;
    Uphi: extended;
    Vr: extended;
    Vz: extended;
    Vphi: extended;
    r: extended;
    z: extended;
    Q: extended;              // ����� �������
    T_in: extended;
  end;

  TPArray = array of TPart;
  TReal2D = array of array of extended;
  TReal1D = array of extended;

  TMain = class(TForm)
    btnStartButton: TButton;
    TimeResultLabel: TLabel;
    Label2: TLabel;
    TimeLabel: TLabel;
    Label4: TLabel;
    btnCancelButton: TButton;
    B0Label: TLabel;
    Alpha0Label: TLabel;
    BadPartsLabel: TLabel;
    lbl1: TLabel;
    edtT1: TEdit;
    edtT2: TEdit;
    lbl2: TLabel;
    lbl3: TLabel;
    lblzmaxt: TLabel;
    lbltmax: TLabel;
    bvl1: TBevel;
    bvl2: TBevel;
    procedure btnStartButtonClick(Sender: TObject);
    procedure init;
    procedure Potential;
    procedure Puasson(Pot, Pot0: TReal2D; QQ: TReal2D);
    procedure Field;
    procedure PartMove;
    procedure Weighing;
    function Er(m: longint): extended;
    function Ez(m: longint): extended;
    procedure btnCancelButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
    New, Old: TPArray;
    alpha: extended;
    QQ, QQSum: TReal2D;
    QQAverage: array of array of Extended;
    QQzmax: array of Extended;
    zRange, rRange, PartNum: longint;
  end;

const
  dz = 0.008;                 // ��� �� ������������
  dr = dz / 2;                // ��� �� �������
  // r0 = 1/8;                // ������ �����
  r1 = 1 / 4;                 // ������ ������
  // n  = round(r0/dr);       // ����� ������ �� �������
  n0 = 10;
  nc = 150000;
  Vbezr0 = 1;                 // ��������� ��������
  zch = dz;                   // ��������� ������� ��� �����������
  rch = dr;                   // ��������� ������� ��� �����������
  dt = dz / n0;               // ��� �� �������
  eps = 0.00001;              // �������� ������� ����������
  PhiL = 0;                   // ��������� �� ����� �������
  PhiR = 0;                   // ��������� �� ������ �������
  Tmax = 7000;                // ����� �����
  beta0 = 0.1;                // ����������
  L = 1;                      // ����� ������������

var
  Main: TMain;
  t, js, ks, err, otrLeft, otrTop, passed, badPart, inN, n: LongInt;
  t1, t2: LongInt;
  zl, zr, rd, ru, deltazl, deltard, eta, B0, alpha0, gamma0, gamma, df, r0: Extended;
  par1, par2, par3, par4: Extended;
  Pot, Pot0, ErF, EzF: TReal2D;
  Stop, StopCheck, CalculationDone: Boolean;
  fEzT1, fEzT2, fEzT3, fTraekt, fErr, fPart, fIn, fQQ, fQQzmax: TextFile;

implementation

{$R *.dfm}

{==========================================================}
function IndexOfMaxValue(x: array of Extended): Integer;
var
  Index: Integer;
  MaxValue: Extended;
begin
  if high(x) = -1 then begin
    Result := 0;
  end else begin
    Result := 0;
    MaxValue := 0;
    for Index := 0 to high(x) do begin
      if x[Index] < MaxValue then begin
        Result := Index;
        MaxValue := x[Index];
      end;
    end;
  end;
end;

function ArrayMaxValue(x: array of Extended): Extended;
var
  Index: Integer;
  MaxValue: Extended;
begin
  if high(x) = -1 then begin
    Result := 0;
  end else begin
    Result := 0;
    MaxValue := 0;
    for Index := 0 to high(x) do begin
      if x[Index] < MaxValue then begin
        Result := x[Index];
        MaxValue := x[Index];
      end;
    end;
  end;
end;

procedure TMain.init;                   // �������������
var
  i, j: longint;
begin

  badPart := 0;                            //����� "������" ������

  par1 := 0.5 / (1 / (dr * dr) + 1 / (dz * dz));     // ��������� ��� ��������� ��������
  par2 := 0.5 / dr;
  par3 := 1 / (dr * dr);
  par4 := 1 / (dz * dz);
  zRange := round(L / dz);                  // ���������� ��������� ����� �� z-����������
  rRange := round(r1 / dr);                 // ���������� ��������� ����� �� r-����������
  alpha := alpha0 * alpha0;
  gamma0 := 1 / sqrt(1 - sqr(beta0));       // ����������

  Stop := true;
  StopCheck := true;
  CalculationDone := false;

  SetLength(Pot, rRange + 1, zRange + 1);   // ������ ������� ������������� ����������
  SetLength(Pot0, rRange + 1, zRange + 1);  // ������� ���������� �� ���������� ����
  SetLength(EzF, rRange + 1, zRange + 1);   // ������ ������� ������������� ����������� ����
  SetLength(ErF, rRange + 1, zRange + 1);   // ������ ������� ������������� ����������� ����
  SetLength(New, nc);                       // ������� ������
  SetLength(Old, nc);
  SetLength(QQ, rRange + 1, zRange + 1);      //������ ������� ������������� ������
  SetLength(QQSum, rRange + 1, zRange + 1);
  SetLength(QQAverage, Tmax + 2, zRange + 1);
  SetLength(QQzmax, Tmax + 2);

  for i := 0 to rRange do                   //���� ������������� ���� ������
    for j := 0 to zRange do
      Pot[i, j] := 0;
  df := PhiL - PhiR;
  for i := 0 to rRange do
  begin
    Pot[i, 0] := PhiL;                       //��������� ������� ��� ����������
    Pot[i, zRange] := PhiR;
  end;
  for j := 1 to zRange do
    Pot[rRange, j] := PhiL                   // ������� �������
  ;
  for i := 0 to rRange do
    for j := 0 to zRange do
      Pot0[i, j] := Pot[i, j];
  for i := 0 to rRange do
    for j := 0 to zRange do
    begin
      EzF[i, j] := 0;                       // ������������� ������� ����������� ����
      ErF[i, j] := 0;                       // ������������� ������� ����������� ����
      QQ[i, j] := 0;
      QQSum[i, j] := 0;
    end;

  PartNum := n;
  for i := 1 to n do
  begin
    Old[i].Vr := 0;
    Old[i].Vz := Vbezr0;
    Old[i].Vphi := (B0 * i * dr) / (2 * gamma0);                 //������� ���� � ������ �����������
    Old[i].Ur := 0;
    Old[i].Uz := Old[i].Vz * gamma0;
    Old[i].Uphi := Old[i].Vphi * gamma0;
    Old[i].z := 0;
    Old[i].r := i * dr;
    Old[i].Q := -(3.14 * dz * dr * (2 * i * dr - dr)) / n0;      //����� ������� (�� �������, ��� ��-�� ������ ���� ������ ���������)
    Old[i].T_in := 0;
  end;
  otrLeft := 0;      // ���-�� ������, ����������� � ��-�� ��������
  otrTop := 0;       // �������� �� ������� ���-��
  passed := 0;       // ���������
  Potential;         // ���������, ������ �����������

  Field;             // ����, ������ �����������
end;

{==========================================================}

procedure TMain.btnStartButtonClick(Sender: TObject);
var
  fName, tmpstr: string;
  ipn, jpn, i, j, iter, partsCounterAvg: longint;
  aL, aR, Da, tmp, zmaxavg, zmaxmax, zmaxmin: extended;
  QQcolumn: array of Extended;
begin
  DecimalSeparator := '.';
  inN := 1;
  t1 := StrToInt(edtT1.text);
  t2 := StrToInt(edtT2.text);
  if t1 < 0 then begin
    ShowMessage('t1 error');
    Exit;
  end;
  if t2 > tmax then begin
    ShowMessage('t2 error');
    Exit;
  end;
  while (inN <= 2) do
  begin
    assignfile(fIn, 'DataInN' + IntToStr(inN) + '.txt');
    reset(fIn);
    readln(fIn, r0);
    n := round(r0 / dr);
    while not EOF(fIn) do
    begin
      readln(fIn, B0, aL, aR, Da);
      alpha0 := aL;
      while (alpha0 <= aR) do
      begin

        init();               //�������������

        btnCancelButton.Enabled := True;
        B0Label.Caption := 'B0=' + FloatToStr(B0);
        Alpha0Label.Caption := 'alpha0=' + FloatToStr(alpha0);

        assignfile(fEzT1, IntToStr(inN) + '\Ez(t),r=0.25r1,z=0.05,B0=' + floatToStr(B0) + ',a=' + floatToStr(alpha0) + ',beta=' + floatToStr(beta0) + '.dat');
        rewrite(fEzT1);

        assignfile(fEzT2, IntToStr(inN) + '\Ez(t),r=0.5r1,z=0.05,B0=' + floatToStr(B0) + ',a=' + floatToStr(alpha0) + ',beta=' + floatToStr(beta0) + '.dat');
        rewrite(fEzT2);

        assignfile(fEzT3, IntToStr(inN) + '\Ez(t),r=0.75r1,z=0.05,B0=' + floatToStr(B0) + ',a=' + floatToStr(alpha0) + ',beta=' + floatToStr(beta0) + '.dat');
        rewrite(fEzT3);

        assignfile(fErr, IntToStr(inN) + '\Err,B0=' + floatToStr(B0) + ',a=' + floatToStr(alpha0) + '.dat');
        rewrite(fErr);

        assignfile(fPart, IntToStr(inN) + '\nOtr,B0=' + floatToStr(B0) + ',a=' + floatToStr(alpha0) + '.dat');
        rewrite(fPart);

        assignfile(fQQzmax, IntToStr(inN) + '\zmax(t).dat');
        rewrite(fQQzmax);
        writeln(fQQzmax, 't   zmax(t)   jmax   QQavg(t)max');

        assignfile(fQQ, IntToStr(inN) + '\QQ(t).dat');
        rewrite(fQQ);

        for t := 1 to Tmax do
        begin
          Application.ProcessMessages();

          DecimalSeparator := '.';
          writeln(fEzT1, IntToStr(t) + ' ' + floattostr(EzF[round(rRange / 4), round(0.05 / dz)]));
          writeln(fEzT2, IntToStr(t) + ' ' + floattostr(EzF[round(2 * rRange / 4), round(0.05 / dz)]));
          writeln(fEzT3, IntToStr(t) + ' ' + floattostr(EzF[round(3 * rRange / 4), round(0.05 / dz)]));

          if (t mod 1000 = 0) then  //������ � ���� ������ �������� �������� (��)
          begin
            assignfile(fTraekt, IntToStr(inN) + '\FP,B0=' + floatToStr(B0) + ',a=' + floatToStr(alpha0) + ',t=' + IntToStr(t) + '.dat');
            rewrite(fTraekt);
            for ipn := 1 to PartNum do
              writeln(fTraekt, FloatToStr(Old[ipn].z) + ' ' + FloatToStr(Old[ipn].r) + ' ' + FloatToStr(Old[ipn].Vz) + ' ' + FloatToStr(Old[ipn].Vr) + ' ' + FloatToStr(Old[ipn].Vphi));
            closefile(fTraekt);
          end;

          if (t mod 500 = 0) then
            label2.Caption := IntToStr(PartNum);

          PartMove;          // ��������� ��������

          Potential;         // ��������� � ��������� ������� ����������

          Field;             // ��������� � ��������� ������� ����

          for ipn := 1 to PartNum do                 // ��������� ������� ���������� ������������ �� z �����.
            if (Old[ipn].z > L) or (Old[ipn].z < 0) then
            begin
              if (Old[ipn].z < 0) then
                Inc(otrLeft);
              if (Old[ipn].z > L) then
                Inc(passed);
              for jpn := ipn to PartNum - 1 do
              begin
                Old[jpn].z := Old[jpn + 1].z;
                Old[jpn].Vr := Old[jpn + 1].Vr;
                Old[jpn].Vz := Old[jpn + 1].Vz;
                Old[jpn].Vphi := Old[jpn + 1].Vphi;
                Old[jpn].r := Old[jpn + 1].r;
                Old[jpn].T_in := Old[jpn + 1].T_in;
                Old[jpn].Q := Old[jpn + 1].Q;
                Old[jpn].Ur := Old[jpn + 1].Ur;
                Old[jpn].Uz := Old[jpn + 1].Uz;
                Old[jpn].Uphi := Old[jpn + 1].Uphi;
              end;
              PartNum := PartNum - 1;
            end;
          for ipn := 1 to PartNum do       // ��������� ������� ���������� ������������ �� r �����.
            if (Old[ipn].r > r1 - dr / 2) or (Old[ipn].r < 0) then
            begin
              if (Old[ipn].r > r1 - dr / 2) then
                Inc(otrTop);
              for jpn := ipn to PartNum - 1 do
              begin
                Old[jpn].z := Old[jpn + 1].z;
                Old[jpn].Vr := Old[jpn + 1].Vr;
                Old[jpn].Vz := Old[jpn + 1].Vz;
                Old[jpn].Vphi := Old[jpn + 1].Vphi;
                Old[jpn].r := Old[jpn + 1].r;
                Old[jpn].T_in := Old[jpn + 1].T_in;
                Old[jpn].Q := Old[jpn + 1].Q;
                Old[jpn].Ur := Old[jpn + 1].Ur;
                Old[jpn].Uz := Old[jpn + 1].Uz;
                Old[jpn].Uphi := Old[jpn + 1].Uphi;
              end;
              PartNum := PartNum - 1;
            end;

          for ipn := PartNum + 1 to PartNum + n do          // ������ ����� �������
          begin
            Old[ipn].Vr := 0;
            Old[ipn].Vz := Vbezr0;
            Old[ipn].Vphi := (B0 * (ipn - PartNum) * dr) / (2 * gamma0);   //������� ���� � ������ �����������
            Old[ipn].Ur := 0;
            Old[ipn].Uz := Old[ipn].Vz * gamma0;
            Old[ipn].Uphi := Old[ipn].Vphi * gamma0;
            Old[ipn].z := 0;
            Old[ipn].r := (ipn - PartNum) * dr;
            Old[ipn].Q := -(3.14 * dz * dr * (2 * (ipn - PartNum) * dr - dr)) / n0;
            Old[ipn].T_in := t;
          end;
          PartNum := PartNum + n;

          TimeResultLabel.Caption := IntToStr(t);
          if (t mod 50 = 0) then
            writeln(fPart, floattostr(otrLeft) + ' ' + floattostr(otrTop) + ' ' + floattostr(passed) + ' ' + floattostr(n * t));

          if (t >= t1) and (t <= t2) then begin
            writeln(fQQ, inttostr(t));
            for i := 0 to rRange do begin
              for j := 0 to zRange do begin
                write(fQQ, floattostr(qq[i][j]) + ' ');
              end;
              writeln(fQQ, '');
            end;
            for j := 0 to zRange do begin
              QQAverage[t][j] := 0;
              tmpstr := '';
              partsCounterAvg := 0;
              for i := 0 to rRange do begin
                QQAverage[t][j] := QQAverage[t][j] + QQ[i][j];
                tmpstr := tmpstr + FloatToStr(QQ[i][j]) + ' ';
                if (QQ[i][j] <> 0) then begin
                  partsCounterAvg := partsCounterAvg + 1;
                end;
                //writeln(fQQzmax, IntToStr(t) + ' ' + floattostr(qq[i][j]));
              end;
              // ������� �������� ���� QQ �� i ��� j
              if (partsCounterAvg > 0) then begin
                QQAverage[t][j] := QQAverage[t][j] / partsCounterAvg;
                //writeln(fQQzmax, IntToStr(t) + ' ' + floattostr(j) + ' ' + floattostr(QQAverage[t][j]) + ' ' + tmpstr);
              end;
            end;
            // ������� zmax = jmax * dz
            QQzmax[t] := IndexOfMaxValue(QQAverage[t]) * dz;
            writeln(fQQzmax, IntToStr(t) + ' ' + floattostr(QQzmax[t]) + ' ' + floattostr(IndexOfMaxValue(QQAverage[t])) + ' ' + floattostr(ArrayMaxValue(QQAverage[t])));
          end;
        end;

        zmaxavg := 0;
        zmaxmax := 0;
        zmaxmin := 1e10;
        for t := t1 to t2 do begin
          if (QQzmax[t] > 0) and (QQzmax[t] > zmaxmax) then
            zmaxmax := QQzmax[t];
          if (QQzmax[t] > 0) and (QQzmax[t] < zmaxmin) then
            zmaxmin := QQzmax[t];
        end;
        zmaxavg := (zmaxmax - zmaxmin) / 2;
        lblzmaxt.Caption := 'zmaxavg=' + floattostr(zmaxavg);
        writeln(fqqzmax, ' ');
        writeln(fQQzmax, 'zmax_width=' + floattostr(zmaxmax - zmaxmin)  + '; zmaxavg=' + floattostr(zmaxavg) + '; zmaxmin=' + floattostr(zmaxmin) + '; zmaxmax=' + floattostr(zmaxmax));

        closefile(fEzT1);
        closefile(fEzT2);
        closefile(fEzT3);
        closefile(fErr);
        closefile(fPart);
        closefile(fQQzmax);
        closefile(fQQ);

        New := nil;
        Old := nil;
        QQ := nil;
        Pot := nil;
        Pot0 := nil;
        ErF := nil;
        EzF := nil;
        QQAverage := nil;
        QQzmax := nil;

        alpha0 := alpha0 + Da;
      end;
    end;
    closefile(fIn);
    Inc(inN);
  end;
  CalculationDone := True;
end;

{==========================================================}

procedure TMain.Potential;
var
  ip, jp: longint;
begin
  Stop := true;
  while Stop do
  begin

    Puasson(Pot, Pot0, QQ);

    StopCheck := true;
    ip := 1;
    while (StopCheck) do
    begin
      for jp := 1 to zRange - 1 do
        if abs(Pot[ip, jp] - Pot0[ip, jp]) > eps then
        begin
          StopCheck := false;
          Stop := true;
          break;
        end
        else
          Stop := false;
      if ip = rRange then
        StopCheck := false;
      Inc(ip);
    end;

    for ip := 0 to rRange do
      for jp := 0 to zRange do
        Pot0[ip, jp] := Pot[ip, jp];

  end;
end;

{==========================================================}

procedure TMain.Puasson(Pot, Pot0: TReal2D; QQ: TReal2D);
var
  i, j: longint;
begin

  for j := 1 to zRange - 1 do
    for i := rRange - 1 downto 1 do
      Pot[i, j] := par1 * ((Pot[i + 1, j] - Pot0[i - 1, j]) * par2 / (dr * i) + (Pot[i + 1, j] + Pot0[i - 1, j]) * par3 +
        (Pot0[i, j + 1] + Pot[i, j - 1]) * par4) - alpha * QQ[i, j] * par1;
  for j := 1 to zRange - 1 do
    Pot[0, j] := Pot[1, j];

end;

{==========================================================}

procedure TMain.Field;
var
  i, j: longint;
begin
  for j := 1 to zRange - 1 do
    for i := rRange - 1 downto 1 do
    begin
      ErF[i, j] := (Pot[i + 1, j] - Pot[i - 1, j]) / (2 * dr);
      EzF[i, j] := (Pot[i, j + 1] - Pot[i, j - 1]) / (2 * dz);
    end;
  for j := 0 to zRange do
    ErF[0, j] := (Pot[1, j] - Pot[0, j]) / dr;
  for j := 0 to zRange do
    ErF[rRange, j] := (Pot[rRange, j] - Pot[rRange - 1, j]) / dr;

  for j := 1 to zRange - 1 do
    EzF[0, j] := (Pot[0, j + 1] - Pot[0, j]) / dz;
  for j := 1 to zRange - 1 do
    EzF[rRange, j] := (Pot[rRange, j + 1] - Pot[rRange, j]) / dz;

  for i := 0 to rRange do
    EzF[i, 0] := (Pot[i, 1] - Pot[i, 0]) / dz;
  for i := 0 to rRange do
    EzF[i, zRange] := (Pot[i, zRange] - Pot[i, zRange - 1]) / dz;

end;

{==========================================================}

procedure TMain.PartMove;
var
  i, intCol, idt, j: longint;
  dtCur, Br, Bz, gTmp, VrT, VzT, VphiT, UrT, UzT, UphiT, rT, zT: extended;
begin
  for i := 1 to PartNum do
  begin
    VrT := Old[i].Vr;
    //���������� ��� ��������� ������� ����� ���������������
    VphiT := Old[i].Vphi;
    VzT := Old[i].Vz;
    UrT := Old[i].Ur;
    UphiT := Old[i].Uphi;
    UzT := Old[i].Uz;
    rT := Old[i].r;
    zT := Old[i].z;
    intCol := 1;
    if (Old[i].r < (2 * dr)) then     //���� ������� ������� ������ � ���, �� ����� �������� ��������������
      intCol := 50;

    repeat                                   // ���� ���-�� � ��������������� ���������
      dtCur := dt / intCol;                  //��� ��������������
      for idt := 1 to intCol do
      begin
        if (Old[i].r > (r1 - dr / 2)) then
          continue;
     { if (Old[i].z<=L) then
      begin
        Bz:=B0*Old[i].z/(L);
        Br:=-0.5*Old[i].r*B0/(L);
      end else
      begin
        Bz:=B0;
        Br:=0;
      end; }
        Bz := B0;
        Br := 0;
        gTmp := sqr(beta0) * (sqr(Old[i].Vr) + sqr(Old[i].Vz) + sqr(Old[i].Vphi));
        if (gTmp < 1) then
          gamma := 1 / sqrt(1 - gTmp)
        else
          break;

        New[i].Ur := (sqr(Old[i].Vphi) * gamma / Old[i].r - (Er(i) + Bz * Old[i].Vphi)) * dtCur + Old[i].Ur;
        New[i].Uphi := (-gamma * Old[i].Vphi * Old[i].Vr / Old[i].r + Bz * Old[i].Vr - Old[i].Vz * Br) * dtCur + Old[i].Uphi;
        New[i].Uz := (-Ez(i) + Old[i].Vphi * Br) * dtCur + Old[i].Uz;       //��������� �������� ����� �������

        New[i].Vr := New[i].Ur / gamma;
        New[i].Vphi := New[i].Uphi / gamma;                //������� �� �������� ��������
        New[i].Vz := New[i].Uz / gamma;

        New[i].z := Old[i].z + New[i].Vz * dtCur;              // ���������� z
        New[i].r := Old[i].r + New[i].Vr * dtCur;              // ���������� r

        if (New[i].r < 0) then
        begin
          New[i].Vr := -New[i].Vr;
          New[i].r := -New[i].r;
        end;

        Old[i].Vr := New[i].Vr;
        Old[i].Vz := New[i].Vz;
        Old[i].Vphi := New[i].Vphi;
        Old[i].Ur := New[i].Ur;
        Old[i].Uz := New[i].Uz;
        Old[i].Uphi := New[i].Uphi;
        Old[i].r := New[i].r;
        Old[i].z := New[i].z;
      end;
      gTmp := sqr(beta0) * (sqr(Old[i].Vr) + sqr(Old[i].Vz) + sqr(Old[i].Vphi));    //�����. ������ ��� ��������������� �������
      if ((gTmp >= 1) and (intCol >= 50)) then     //���� ������� "������" � �������� ������ � ���-��, �� �� �������
      begin
        writeln(fErr, floattostr(Old[i].Vr) + ' ' + floattostr(Old[i].Vphi) + ' ' + floattostr(Old[i].Vz) + ' ' +
          floattostr(Old[i].r));  // �� ������� � �������������� � ����
        Old[i].r := r1 + 2 * dr;       // � "�����������" � �� �������
        Inc(badPart);
        BadPartsLabel.Caption := 'badPart=' + IntToStr(badPart);
        break;
      end;
      if ((gTmp >= 1) and (intCol < 50)) then   //���� ������� "������", �� ����������� �������� ���-�� (intCol)
      begin
        Old[i].Vr := VrT;       //� ��� �������������� ������� ���������� ����� (�� ������� ���-��)
        Old[i].Vz := VzT;
        Old[i].Vphi := VphiT;
        Old[i].Ur := UrT;
        Old[i].Uz := UzT;
        Old[i].Uphi := UphiT;
        Old[i].r := rT;
        Old[i].z := zT;
        Inc(intCol);
      end;
    until (gTmp < 1);         //������� �� ����� �����, ����� ������� ��������� ����������������
  end;

  for i := 0 to rRange do
    for j := 0 to zRange do
      QQ[i, j] := 0;

  Weighing;                   // ����������� ������ �� �����

end;

{==========================================================}

procedure TMain.Weighing;
var
  m: longint;
  Q0: extended;
begin
  for m := 1 to PartNum do
  begin
    Q0 := Old[m].Q / abs((3.14 * dz * dr * (2 * New[m].r - dr)));
    //��������� ������ ������� (����� ������� ����� �� ������� ����� �������)

    zl := New[m].z;
    zr := New[m].z + dz;
    rd := New[m].r - dr;
    ru := New[m].r;

    js := round(New[m].z / dz);
    ks := round(New[m].r / dr);

    deltazl := New[m].z - js * dz;
    deltard := New[m].r - ks * dr;

    if (New[m].z >= 0) and (ks >= 0) and (ks <= rRange) and (js <= zRange) then
      // ����������� ������ �� �����
    begin
      QQ[ks, js] := QQ[ks, js] + Q0 * ((dz / 2 - deltazl) * (dr / 2 + deltard) / (zch * rch));
      if (js + 1 <= zRange) then
        QQ[ks, js + 1] := QQ[ks, js + 1] + Q0 * ((dz / 2 + deltazl) * (dr / 2 + deltard) / (zch * rch));

      if (ks - 1) >= 0 then
        QQ[ks - 1, js] := QQ[ks - 1, js] + Q0 * ((dz / 2 - deltazl) * (dr / 2 - deltard) / (zch * rch));
      if (ks - 1) < 0 then
        QQ[1, js] := QQ[1, js] + Q0 * ((dz / 2 - deltazl) * (dr / 2 - deltard) / (zch * rch));

      if ((js + 1 <= zRange) and ((ks - 1) >= 0)) then
        QQ[ks - 1, js + 1] := QQ[ks - 1, js + 1] + Q0 * ((dz / 2 + deltazl) * (dr / 2 - deltard) / (zch * rch));
      if ((js + 1 <= zRange) and ((ks - 1) < 0)) then
        QQ[1, js + 1] := QQ[1, js + 1] + Q0 * ((dz / 2 + deltazl) * (dr / 2 - deltard) / (zch * rch));
    end;

  end;
end;

{==========================================================}

function TMain.Er(m: longint): extended;
var
  tmp: extended;

begin
  tmp := 0;
  // ����������� ���� �� �������

  zl := Old[m].z;
  zr := Old[m].z + dz;
  rd := Old[m].r - dr;
  ru := Old[m].r;

  js := round(Old[m].z / dz);
  ks := round(Old[m].r / dr);

  deltazl := Old[m].z - js * dz;
  deltard := Old[m].r - ks * dr;

  if (Old[m].z >= 0) and (ks >= 0) and (ks <= rRange) and (js <= zRange) then
    // ����������� ������ �� �����
  begin
    tmp := tmp + ErF[ks, js] * ((dz / 2 - deltazl) * (dr / 2 + deltard) / (zch * rch));
    if (js + 1 <= zRange) then
      tmp := tmp + ErF[ks, js + 1] * ((dz / 2 + deltazl) * (dr / 2 + deltard) / (zch * rch));
    if (ks - 1) >= 0 then
      tmp := tmp + ErF[ks - 1, js] * ((dz / 2 - deltazl) * (dr / 2 - deltard) / (zch * rch));
    if (ks - 1) < 0 then
      tmp := tmp + ErF[ks, js] * ((dz / 2 - deltazl) * (dr / 2 - deltard) / (zch * rch));
    if ((js + 1 <= zRange) and ((ks - 1) >= 0)) then
      tmp := tmp + ErF[ks - 1, js + 1] * ((dz / 2 + deltazl) * (dr / 2 - deltard) / (zch * rch));
    if ((js + 1 <= zRange) and ((ks - 1) < 0)) then
      tmp := tmp + ErF[ks, js + 1] * ((dz / 2 + deltazl) * (dr / 2 - deltard) / (zch * rch));
  end;
  Er := tmp;
end;

{==========================================================}

function TMain.Ez(m: longint): extended;
var
  tmp: extended;
begin
  tmp := 0;

  zl := Old[m].z;
  zr := Old[m].z + dz;
  rd := Old[m].r - dr;
  ru := Old[m].r;

  js := round(Old[m].z / dz);
  ks := round(Old[m].r / dr);

  deltazl := Old[m].z - js * dz;
  deltard := Old[m].r - ks * dr;

  if (Old[m].z >= 0) and (ks >= 0) and (ks <= rRange) and (js <= zRange) then
    // ����������� ������ �� �����
  begin
    tmp := tmp + EzF[ks, js] * ((dz / 2 - deltazl) * (dr / 2 + deltard) / (zch * rch));
    if (js + 1 <= zRange) then
      tmp := tmp + EzF[ks, js + 1] * ((dz / 2 + deltazl) * (dr / 2 + deltard) / (zch * rch));
    if (ks - 1) >= 0 then
      tmp := tmp + EzF[ks - 1, js] * ((dz / 2 - deltazl) * (dr / 2 - deltard) / (zch * rch));
    if (ks - 1) < 0 then
      tmp := tmp + EzF[ks, js] * ((dz / 2 - deltazl) * (dr / 2 - deltard) / (zch * rch));
    if ((js + 1 <= zRange) and ((ks - 1) >= 0)) then
      tmp := tmp + EzF[ks - 1, js + 1] * ((dz / 2 + deltazl) * (dr / 2 - deltard) / (zch * rch));
    if ((js + 1 <= zRange) and ((ks - 1) < 0)) then
      tmp := tmp + EzF[ks, js + 1] * ((dz / 2 + deltazl) * (dr / 2 - deltard) / (zch * rch));
  end;

  Ez := tmp;
end;

{==========================================================}
procedure TMain.btnCancelButtonClick(Sender: TObject);
begin
  if (not CalculationDone) then begin
    closefile(fEzT1);
    closefile(fEzT2);
    closefile(fEzT3);
    closefile(fErr);
    closefile(fPart);
    closefile(fQQzmax);
    closefile(fQQ);
    System.Halt(0);
  end else
    Close();
end;

procedure TMain.FormCreate(Sender: TObject);
begin
  SetThreadLocale(1049);
  DecimalSeparator := '.';
  lbltmax.Caption := 'Tmax=' + FloatToStr(tmax);
end;

end.
