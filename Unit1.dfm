object Main: TMain
  Left = 1556
  Top = 277
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'AutoVers'
  ClientHeight = 244
  ClientWidth = 315
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object bvl2: TBevel
    Left = 8
    Top = 152
    Width = 217
    Height = 81
    Shape = bsFrame
  end
  object bvl1: TBevel
    Left = 8
    Top = 32
    Width = 177
    Height = 113
    Shape = bsFrame
  end
  object lbl1: TLabel
    Left = 24
    Top = 8
    Width = 269
    Height = 16
    Caption = #1044#1074#1091#1084#1077#1088#1085#1086#1077' '#1084#1086#1076#1077#1083#1080#1088#1086#1074#1072#1085#1080#1077' '#1076#1080#1085#1072#1084#1080#1082#1080' '#1042#1050
  end
  object TimeResultLabel: TLabel
    Left = 125
    Top = 45
    Width = 7
    Height = 16
    Caption = '0'
  end
  object Label2: TLabel
    Left = 125
    Top = 64
    Width = 7
    Height = 16
    Caption = '0'
  end
  object B0Label: TLabel
    Left = 20
    Top = 82
    Width = 23
    Height = 16
    Caption = 'B0='
  end
  object Alpha0Label: TLabel
    Left = 20
    Top = 99
    Width = 41
    Height = 16
    Caption = 'alpha='
  end
  object BadPartsLabel: TLabel
    Left = 20
    Top = 117
    Width = 48
    Height = 16
    Caption = 'badPart'
  end
  object lbl2: TLabel
    Left = 20
    Top = 164
    Width = 23
    Height = 16
    Caption = 't1 = '
  end
  object lbl3: TLabel
    Left = 20
    Top = 196
    Width = 23
    Height = 16
    Caption = 't2 = '
  end
  object lblzmaxt: TLabel
    Left = 100
    Top = 197
    Width = 49
    Height = 16
    Caption = 'zmax(t)='
  end
  object lbltmax: TLabel
    Left = 100
    Top = 160
    Width = 42
    Height = 16
    Caption = 'lbltmax'
  end
  object TimeLabel: TLabel
    Left = 20
    Top = 43
    Width = 44
    Height = 16
    Caption = #1042#1088#1077#1084#1103':'
  end
  object Label4: TLabel
    Left = 20
    Top = 62
    Width = 92
    Height = 16
    Caption = #1063#1080#1089#1083#1086' '#1095#1072#1089#1090#1080#1094':'
  end
  object btnStartButton: TButton
    Left = 208
    Top = 34
    Width = 97
    Height = 39
    Caption = #1055#1091#1089#1082
    TabOrder = 2
    TabStop = False
    OnClick = btnStartButtonClick
  end
  object btnCancelButton: TButton
    Left = 208
    Top = 103
    Width = 97
    Height = 42
    Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1080' '#1074#1099#1081#1090#1080
    Enabled = False
    TabOrder = 3
    WordWrap = True
    OnClick = btnCancelButtonClick
  end
  object edtT1: TEdit
    Left = 44
    Top = 160
    Width = 49
    Height = 24
    TabOrder = 0
    Text = '100'
  end
  object edtT2: TEdit
    Left = 44
    Top = 192
    Width = 49
    Height = 24
    TabOrder = 1
    Text = '102'
  end
end
